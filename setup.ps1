# Inital bootstrapping of wsl to download this repo and setup cloud-init
# to start creating a wsl instance

Write-Output "[WSL] Checking for .cloud-init folder."
# First ensure .clo ud-init folder is present
if (-Not (Test-Path -Path $HOME/.cloud-init)) {
    New-Item -ItemType Directory -Path $HOME/.cloud-init | Out-Null
    Write-Output "[WSL] .cloud-init folder created."
} else {
    Write-Output "[WSL] .cloud-init folder is already present"
}

$wsl_clean_ps1 = "wsl_clean.ps1"
$wsl_ubuntu_ps1 = "wsl_ubuntu.ps1"
$wsl_cloud_init = "Ubuntu-Preview.user-data"

$powershell_location = "https://codeberg.org/marcbosserhoff/wsl/raw/branch/master/powershell"
$cloud_init_location = "https://codeberg.org/marcbosserhoff/wsl/raw/branch/master/.cloud-init"

Write-Output "[WSL] Downloading $wsl_clean_ps1..."
Invoke-WebRequest -Uri $powershell_location/$wsl_clean_ps1 -OutFile ./$wsl_clean_ps1

Write-Output "[WSL] Downloading $wsl_ubuntu_ps1..."
Invoke-WebRequest -Uri $powershell_location/$wsl_ubuntu_ps1 -OutFile ./$wsl_ubuntu_ps1

Write-Output "[WSL] Downloading .cloud-init configuration..."
Invoke-WebRequest -Uri $cloud_init_location/$wsl_cloud_init -OutFile "$HOME/.cloud-init/$wsl_cloud_init"
