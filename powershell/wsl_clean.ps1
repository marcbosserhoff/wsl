# Remove Ubuntu on WSL to cleanup workspace or restart provisioning

Write-Host "You will be removing Ubuntu-Preview permanently, all unsaved data is lost..."

$confirmation = Read-Host "Are you sure you want to proceed with this operation? (Y/N)"
if ($confirmation -match '^[Yy]$') {
    Write-Host "Removing Ubuntu-Preview permanently..."
    wsl --unregister Ubuntu-Preview
}
