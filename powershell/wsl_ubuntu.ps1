# Startup Ubuntu on WSL and clone workspace repo into local home folder

# Basic installation
ubuntupreview.exe install --root

# Run cloud-init to have user "workstation" and home folder setup
wsl -d Ubuntu-Preview cloud-init status --wait

# Clone this repository to home folder with correct user rights
wsl -d Ubuntu-Preview -u workstation git clone https://codeberg.org/marcbosserhoff/workstation.git /home/workstation/workstation

# Rewrite git remote as we later have an SSH we can configure inside codeberg to contribute to that repository
# Only possible if youre a member of the project
wsl -d Ubuntu-Preview -u workstation --cd /home/workstation/workstation git remote set-url origin git@codeberg.org:marcbosserhoff/workstation.git

# Setup linux specific stuff afterwards to have fully running linux at hand
wsl -d Ubuntu-Preview -u workstation /home/workstation/workstation/bootstrap/setup.sh

# Setup add specific stuff needed for WSL
wsl -d Ubuntu-Preview -u workstation /home/workstation/workstation/bootstrap/wsl/install.sh

# Terminate vm to reboot, otherwise the changed before are not correctly applied
wsl -t Ubuntu-Preview
