Install WSL Ubuntu
==================

To allow the execution of powershell scripts:

Open Terminal as Administrator, run "powershell" and execute:

    Set-ExecutionPolicy RemoteSigned -Scope CurrentUser

This allows the execution of powershell scripts locally.

Then we need to do very basic WSL bootstrapping to get this setup script
downloaded to be able to run the other scripts. Run the following command:

    Invoke-WebRequest -Uri https://codeberg.org/marcbosserhoff/wsl/raw/branch/master/setup.ps1 -Outfile setup.ps1

and then run

    ./setup.ps1

Now we can setup the Ubnutu machine with cloud-init:

    .\wsl_clean.ps1
    .\wsl_ubuntu.ps1

After that you can just

    wsl

into your box, being in the home folder and have the workstation project already checked out.